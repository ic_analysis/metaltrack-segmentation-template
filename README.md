# PipelineTemplate
Template for the user submission for IC Analysis

# requirements.txt
This already contains all libraries that will be installed on our containers by default. You may add new libraries.
You may remove the existing content of the file but beware of adding dependencies that aren't compatible with the existing ones.

# solution.py
This file defines the function that will be benchmarked and judged by the IC Analysis platform. The format should be strictly followed.

# official IC analysis gitlab account
@icanalysis
icanalysis.tuini06@tum.de
